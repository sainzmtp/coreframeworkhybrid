package utilities.ExtentReports;

import com.relevantcodes.extentreports.ExtentReports;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ExtentManager {
    private static String txtFecha = "yyyy-MM-dd-HHmmss";
    private static String txtRuta = "src/reports/Reporte-BancomerMovilHibryd";
    private static String txtHtml = ".html";
    private static ExtentReports extent;

    public synchronized static ExtentReports getReporter() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(txtFecha);
        LocalDateTime now = LocalDateTime.now();
        String fecha = dtf.format(now);

        if (extent == null) {
            /**Set HTML reporting file location String workingDir = System.getProperty("user.dir");**/
            extent = new ExtentReports(txtRuta + fecha + txtHtml);
        }
        return extent;
    }
}

package utilities.Listeners;

import com.relevantcodes.extentreports.LogStatus;
import manage.ThreadLocalDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utilities.ExtentReports.ExtentManager;
import utilities.ExtentReports.ExtentTestManager;

import java.util.Random;

import static utilities.Tools.waitTime;


public class TestListener extends ThreadLocalDriver implements ITestListener {
    private String txtStartMethod = "... [ INICIANDO ]";
    private String txtTestsucces = "... [ FINALIZADO CON EXITO ]";
    private String txtTestFinished = "Finished Case Test ";
    private String txtWebDriver = "WebDriver";
    private String ruta = "data:image/png;base64,";
    private String txtTestExitoso = "Test Exitoso: ";
    private String txtTestFailed = " Test Failed: ";
    private String txtTestSkipped = " Test Skipped: ";
    private String txtStartTest = "Started Case Test: ";


    private String getTestMethodName(ITestResult iTestResult) {
        //Random rand = new Random();
        return iTestResult.getMethod().getConstructorOrMethod().getName();

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        System.out.println(txtStartTest + iTestContext.getName() + "\n");
        iTestContext.setAttribute(txtWebDriver, getTLDriver());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        System.out.println(txtTestFinished + iTestContext.getName() + "\n");
        /**Do tier down operations for extentreports reporting!*/
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println(getTestMethodName(iTestResult) + txtStartMethod + "\n");

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println(getTestMethodName(iTestResult) + txtTestsucces + "\n");
        waitTime(2000);
        /**Extentreports log operation for passed tests.**/
        Random rand = new Random();
        int x = rand.nextInt(100);
        String base64Screenshot = ruta + ((TakesScreenshot) ThreadLocalDriver.getTLDriver()).getScreenshotAs(OutputType.BASE64);
        ExtentTestManager.getTest().log(LogStatus.PASS, ExtentTestManager.getTest().getDescription(), ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {

        System.out.println(txtTestFailed + getTestMethodName(iTestResult) + "\n");
        /**Extentreports log and screenshot operations for failed tests.**/
        Random rand = new Random();
        int x = rand.nextInt(100);
        ExtentTestManager.getTest().log(LogStatus.FAIL, getTestMethodName(iTestResult) + txtTestFailed);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println(txtTestSkipped + getTestMethodName(iTestResult) + "\n");
        /**Extentreports log operation for skipped tests.**/
       // Random rand = new Random();
        //int x = rand.nextInt(1000);
        ExtentTestManager.getTest().log(LogStatus.SKIP, getTestMethodName(iTestResult) + txtTestSkipped);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        String txt = "Test failed but it is in defined success ratio ";
        System.out.println(txt + getTestMethodName(iTestResult));
    }
}

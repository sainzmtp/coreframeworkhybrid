package utilities.Listeners;

import com.relevantcodes.extentreports.LogStatus;
import manage.ThreadLocalDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import utilities.ExtentReports.ExtentTestManager;

public class Retry implements IRetryAnalyzer {
    private static int maxTry = 1;
    private String ruta = "data:image/png;base64,";
    private String txtFallido = "Test Failed";
    /**
     * Run the failed Login 2 times
     **/
    private int count = 0;

    @Override
    public boolean retry(ITestResult iTestResult) {
        if (!iTestResult.isSuccess()) {                      /**Check if Login not succeed**/
            if (count < maxTry) {                            /**Check if maxtry count is reached**/
                count++;                                     /**Increase the maxTry count by 1**/
                iTestResult.setStatus(ITestResult.FAILURE);  /**Mark Login as failed**/
                extendReportsFailOperations();    /**ExtentReports fail operations**/
                return true;                                 /**Tells TestNG to re-ru n the Login**/
            }
        } else {
            iTestResult.setStatus(ITestResult.SUCCESS);      /**If Login passes, TestNG marks it as passed**/
        }
        return false;
    }

    public void extendReportsFailOperations() {
        String base64Screenshot = ruta + ((TakesScreenshot) ThreadLocalDriver.getTLDriver()).getScreenshotAs(OutputType.BASE64);
        ExtentTestManager.getTest().log(LogStatus.FAIL, txtFallido, ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
    }
}
package utilities;

import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import manage.ThreadLocalDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import utilities.ExtentReports.ExtentTestManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.err;
import static manage.ThreadLocalDriver.getTLDriver;


public class Tools {

    static String objecto = "";
    static String nomElement = "";
    static String objetoEncontrado = "Objeto encontrado: ";
    static String objetoNoEncontrado = "Objeto no encontrado";
    static String txtLosTextosSonIgualesLos = "textos son iguales";
    static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss");
    static LocalDateTime now = LocalDateTime.now();
    private static String fecha = dtf.format(now);

    private static AppiumDriver<?> driver = getTLDriver();
    private static TouchAction action = new TouchAction(driver);
    private static int HEIGHT_SCREEN = driver.manage().window().getSize().getHeight();
    private static final int yOffset = HEIGHT_SCREEN / 2;
    private static int WIDTH_SCREEN = driver.manage().window().getSize().getWidth();
    private static final int xOffset = WIDTH_SCREEN / 2;


    private TouchAction touch;
           public void verificarMapeoDeElemento(List<MobileElement> element,String nombreElemento){
            int cantidad =element.size();

            if(cantidad>=1){
                String elementoEncontrado = String.valueOf(element.get(0));
                int inicio = elementoEncontrado.indexOf("xcodeSigningId");
                String Locator =elementoEncontrado.substring(inicio + 1);
                System.err.print(nombreElemento+ "\n "+ cantidad+ " elemento(s) encontrado(s).\n 1er locator encontrado: "+Locator+"\n");
                if(cantidad==2){
                    elementoEncontrado = String.valueOf(element.get(1));
                    System.err.print("\n 2do locator encontrado: "+Locator+"\n");
                }
                if(cantidad==3){
                    elementoEncontrado = String.valueOf(element.get(2));
                    System.err.print("\n 3er locator encontrado: "+Locator+"\n");
                }
                if(cantidad==4){
                    elementoEncontrado = String.valueOf(element.get(3));
                    System.err.print("\n 4to locator encontrado: "+Locator+"\n");
                }
                if(cantidad==5){
                    elementoEncontrado = String.valueOf(element.get(4));
                    System.err.print("\n 5to locator encontrado: "+Locator+"\n");
                }

            }else {
                System.err.println("\n "+nombreElemento+" Tiene Cero coincidencias , es necesario remapear");
            }


        }
    public static void swipeUPDown() {
        action.press(PointOption.point(xOffset, yOffset))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
                .moveTo(PointOption.point(xOffset, HEIGHT_SCREEN / 6))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
                .moveTo(PointOption.point(xOffset, HEIGHT_SCREEN / 2))
                .release().perform();
    }

    public static void waitTime(int time) {

        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitElementIndividual(MobileElement element) {
        String Esperando = "esperando al elemento";
        do {
            System.out.println(Esperando);
            waitTime(1000);
        } while (!element.isDisplayed());

    }

    public static String validTextIndividual(MobileElement object, String text) {
        String result;

        try {
            Assert.assertEquals(object.getText(), text);

        } catch (AssertionError e) {
            String erroneo = "Fue erroneo el caso";
            System.err.println(erroneo);
            String ruta = "data:image/png;base64,";
            String base64Screenshot = ruta + ((TakesScreenshot) getTLDriver()).
                    getScreenshotAs(OutputType.BASE64);

            ExtentTestManager.getTest().log(LogStatus.FAIL, e.getMessage(),
                    ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
        }
        result = txtLosTextosSonIgualesLos;
        return result;
    }

    public static String validText(List<MobileElement> object, String text) {
        String result;
        MobileElement a = object.get(0);
        try {
            Assert.assertEquals(a, text);

        } catch (AssertionError e) {
            String erroneo = "Fue erroneo el caso";
            System.err.println(erroneo);
            String ruta = "data:image/png;base64,";
            String base64Screenshot = ruta + ((TakesScreenshot) getTLDriver()).
                    getScreenshotAs(OutputType.BASE64);

            ExtentTestManager.getTest().log(LogStatus.FAIL, e.getMessage(),
                    ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
        }
        result = txtLosTextosSonIgualesLos;
        return result;
    }

    public static String validText2(String textA, String textB) {
        String result;
        try {
            Assert.assertEquals(textA, textB);
        } catch (AssertionError e) {
            String erroneo = "Fue erroneo el caso";
            System.err.println(erroneo);

            String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) getTLDriver()).
                    getScreenshotAs(OutputType.BASE64);

            ExtentTestManager.getTest().log(LogStatus.FAIL, e.getMessage(),
                    ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
        }
        result = txtLosTextosSonIgualesLos;

        return result;
    }
    public static boolean isPresentIndividual(MobileElement object) {
        boolean found;
        try {
            if (object.isDisplayed()) {
                objecto = object.getText();
                System.out.print("\n"+ objecto +"\n");
            }
            found = true;

        } catch (Exception e) {
            System.out.println(objetoNoEncontrado + objecto);
            found = false;
        }
        return found;
    }

    public static boolean isPresentArreglo(List<MobileElement> object) {
        boolean found = false;
        if (object == null) {
            String vacio = "Objecto Vacio";
            System.err.print(vacio);
        } else {
            try {
                for (MobileElement element : object) {
                    if (element.isDisplayed()) {
                        nomElement = element.getText();
                    } else {
                        continue;
                    }

                }
                System.out.println(objetoEncontrado + nomElement);
                found = true;
            } catch (Exception e) {

                System.out.println(objetoNoEncontrado);
                found = false;
            }

        }
        return found;
    }

    public static void waitElement(List<MobileElement> elemento) {
        if (elemento == null) {
            String vacio = "Objecto Vacio";
            System.err.print(vacio);
        } else {
            try {
                for (MobileElement element : elemento) {
                    do {

                        String Esperando = "esperando al elemento";
                        System.out.println(Esperando);
                    } while (!element.isEnabled());

                }
                System.out.println(objetoEncontrado + nomElement);
            } catch (Exception e) {

            }

        }
    }

    public void mostrarBarraDeHerramientas() {
        touch = new TouchAction(ThreadLocalDriver.getTLDriver());
        Dimension celularWindowSize = ThreadLocalDriver.getTLDriver().manage().window().getSize();
        System.out.print("Dimenciones del cel" + celularWindowSize);
        Duration duracion = Duration.ofMillis(500);

        touch.press(PointOption.point((int) (celularWindowSize.width * 0.5), (int) (celularWindowSize.height * 0.9999)))
                .waitAction(WaitOptions.waitOptions(duracion))
                .moveTo(PointOption.point((int) (celularWindowSize.width * 0.5), (int) (celularWindowSize.height * 0.5)))
                .release().perform();
    }
    public static boolean isDisabled(MobileElement object) {
        boolean found;

        try {
            object.isEnabled();
            String objectoNoHabilitado = "Objeto no habilitado";
            System.err.println(objectoNoHabilitado);

            found = false;

        } catch (Exception e) {
            System.err.println(objetoNoEncontrado);
            found = true;
        }
        return found;
    }
    public void ocultarBarraDeHerramientas() {

        touch = new TouchAction(ThreadLocalDriver.getTLDriver());
        Dimension celularWindowSize = ThreadLocalDriver.getTLDriver().manage().window().getSize();
        System.out.print("Dimenciones del cel" + celularWindowSize);
        Duration duracion = Duration.ofMillis(300);
        touch.press(PointOption.point((int) (celularWindowSize.width * 0.5), (int) (celularWindowSize.height * 0.1)))
                .waitAction(WaitOptions.waitOptions(duracion))
                .moveTo(PointOption.point((int) (celularWindowSize.width * 0.5), (int) (celularWindowSize.height * 0.3)))
                .release().perform();
    }

    public synchronized boolean wifiSetup(String udid, boolean flg) {
        synchronized (this) {
            String flgEnabled = (flg) ? "enable" : "disable";

            List<String> output = Console.runProcess(false, "adb -s " + udid + " shell am broadcast -a io.appium.settings.wifi --es setstatus " + flgEnabled);
            for (String line : output) {
                err.println(line);
                if (line.equalsIgnoreCase("Broadcast completed: result=-1"))
                    return true;
            }
            return false;
        }
    }

    public static class Console {
        private static final String[] WIN_RUNTIME = {"cmd.exe", "/C"};
        private static final String[] OS_LINUX_RUNTIME = {"/bin/bash", "-l", "-c"};

        private Console() {

        }

        private static <T> T[] concat(T[] first, T[] second) {
            T[] result = Arrays.copyOf(first, first.length + second.length);
            System.arraycopy(second, 0, result, first.length, second.length);
            return result;
        }


        public static List<String> runProcess(boolean isWin, String... command) {
            String[] allCommand = null;
            if (isWin) {
                allCommand = concat(WIN_RUNTIME, command);
            } else {
                allCommand = concat(OS_LINUX_RUNTIME, command);
            }
            try {
                ProcessBuilder pb = new ProcessBuilder(allCommand);
                pb.redirectErrorStream(true);
                Process p = pb.start();
                p.waitFor();
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String _temp = null;
                List<String> line = new ArrayList<String>();
                while ((_temp = in.readLine()) != null) {
                    line.add(_temp);
                }
                return line;

            } catch (Exception e) {
                return null;
            }
        }


        @SuppressWarnings({"hiding"})
        private <String> String[] concatStr(String[] first, String[] second) {
            String[] result = Arrays.copyOf(first, first.length + second.length);
            System.arraycopy(second, 0, result, first.length, second.length);
            return result;
        }

        public String validTextNotEqual(MobileElement object, String text) {
            String result;
            if (object.getText().equals(text)) {
                result = txtLosTextosSonIgualesLos;
            } else {
                result = "Los textos no coinciden: Esperado: " + object.getText() + " Obtenido: " + text;
            }
            System.err.println(result);

            return result;
        }
    }
   /* public static void swipeUp() {
        action.press(PointOption.point(xOffset, yOffset))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
                .moveTo(PointOption.point(xOffset, HEIGHT_SCREEN / 6))
                .release()
                .perform();
    }

    public static String validCellphoneHide(MobileElement element, String text) {
        String result;

        if (element.getText().contains(text)) {
            result = txtLosTextosSonIgualesLos;
        } else {
            String text1 = "Los textos no coinciden: Esperado: ";
            String text2 = " Obtenido: ";
            result = text1 + element.getText() + text2 + text;
        }
        System.err.println("\n" + result + "\n");

        return result;
    }

    public static void swipeUpTransferencias() {
        action.press(PointOption.point(xOffset, yOffset))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
                .moveTo(PointOption.point(xOffset, HEIGHT_SCREEN / 5))
                .release()
                .perform();
    }

    public static void swipeUpTransferencias1() {
        action.press(PointOption.point(xOffset, yOffset))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
                .moveTo(PointOption.point(xOffset, HEIGHT_SCREEN / 15))
                .release()
                .perform();
    }

    public static void swipeUpTransferencias2() {
        action.press(PointOption.point(xOffset, yOffset))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
                .moveTo(PointOption.point(xOffset, HEIGHT_SCREEN / 1))
                .release()
                .perform();
    }

    public static void captureScreenShot(String name) {

        try {
            TakesScreenshot ts = getTLDriver();
            String ruta1 = "src/screenshots/";
            String screenshotsFolder = ruta1 + fecha;

            File directory = new File(screenshotsFolder);

            if (!directory.exists()) {
                directory.mkdir();
            } else {
                System.setErr(System.out);
            }

            //FileHandler.copy(ts.getScreenshotAs(OutputType.FILE), new File(screenshotsFolder + Data.deviceName +"-" + nombreCP+"-"+name + ".png"));
            FileHandler.copy(ts.getScreenshotAs(OutputType.FILE), new File(screenshotsFolder + "-" + name + ".png"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static boolean isEnabled(MobileElement object) {
        boolean found;

        try {
            object.isEnabled();
            String ElementoHabilitado = "elemento habilitado";
            System.out.println(ElementoHabilitado);
            found = true;
        } catch (Exception e) {
            found = false;
        }
        return found;
    }



    public static String convertMonth(String month) {
        String monthR = "";
        switch (month) {
            case "enero":
                monthR = "01";
                break;
            case "febrero":
                monthR = "02";
                break;
            case "marzo":
                monthR = "03";
                break;
            case "abril":
                monthR = "04";
                break;
            case "mayo":
                monthR = "05";
                break;
            case "junio":
                monthR = "06";
                break;
            case "julio":
                monthR = "07";
                break;
            case "agosto":
                monthR = "08";
                break;
            case "septiembre":
                monthR = "09";
                break;
            case "octubre":
                monthR = "10";
                break;
            case "noviembre":
                monthR = "11";
                break;
            case "diciembre":
                monthR = "12";
                break;
            default:
                break;
        }

        return monthR;

    }*/
}


package utilities;

import io.appium.java_client.AppiumDriver;
import manage.ThreadLocalDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import utilities.ExtentReports.ExtentTestManager;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static manage.Service.capability;
import static utilities.Tools.waitTime;

public class Data {

    AppiumDriver<?> app;

    @AfterClass
    public void endTest() {
        ExtentTestManager.endTest();
    }

    @BeforeClass
    @Parameters({"browserName", "deviceName", "platformName", "platformVersion", "udid", "automationName", "port", "author", "descripcion", "wdaLocalPort", "category", "appPackage", "activity", "bundleId", "llavero","user","SystemPort"})
    public synchronized void beforeClass(String browserName, String deviceName, String platformName, String platformVersion, String udid, String automationName, String port, String autor, String descripcion, String wdaLocalPort, String categoria, String appPackage, String activity, String bundleId, String llavero, String user,String SystemPort) throws Exception {

        ExtentTestManager.startTest(deviceName + getClass().getSimpleName(), descripcion);
        ExtentTestManager.getTest().assignAuthor(autor);
        ExtentTestManager.getTest().assignCategory(categoria);
        //String CPActual;
        /*List<String> CP = new ArrayList<String>();
        CP.add("LoginConUsuarioNuevo");
        CP.add("LoginMensajeDeErrorClaveDeAcceso");
        CP.add("MensajeErrorUser");
        CP.add("LoginYCierreSesion");*/
      //  CPActual = String.valueOf(ExtentTestManager.getTest().getTest().getName());
        //runtime.exec("adb shell pm clear com.bbva.GEMA");
        //runtime.exec("pm clear com.bbva.GEMA");
        //waitTime(2000);
        /*for (String name : CP) {
            if(CPActual.contains(name)){
                runtime.exec("adb shell pm clear com.bbva.GEMA");
                break;
            }
        }*/

        String deleteCmd = "adb -s "+udid+" shell pm clear com.bbva.GEMA";
        Runtime runtime = Runtime.getRuntime();
        try {
           // automationName="UIAutomator1";
            Thread.sleep(3000);
            runtime.exec(deleteCmd);
            Thread.sleep(3000);
            //waitTime(3000);
        } catch (IOException e) {
            e.printStackTrace();
        }



        if (ThreadLocalDriver.getTLDriver() == null) {
            /*Process process = Runtime.getRuntime().exec("adb shell pm clear com.bbva.GEMA");*/

            app = capability(browserName, deviceName, platformName, platformVersion, udid, automationName, port, wdaLocalPort, appPackage, activity, bundleId, llavero,SystemPort);
            //app.resetApp();

            /*runtime.exec(new String[] {"/usr/bin/adb", "shell pm clear com.bbva.GEMA"});*/

            System.out.print("----------------------SE INICIA EL DRIVER. " + deviceName + "\n");
        } else {
            System.out.print("---------------------SE INICIA LA APP SIN APLICAR DRIVER  " + deviceName + "\n");
            ThreadLocalDriver.getTLDriver().launchApp();

        }


    }

    @AfterMethod
    @Parameters({"deviceName", "category"})
    public synchronized void afterMethod(String deviceName, Method m, String categoria) {
        waitTime(4000);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss");
        LocalDateTime now = LocalDateTime.now();
        String fecha = dtf.format(now);

        String screenshotsFolder = "src/screenshots/" + deviceName;

        File directory = new File(screenshotsFolder);

        if (!directory.exists()) {
            directory.mkdir();
        }

        File file = ThreadLocalDriver.getTLDriver().getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file, new File("src/screenshots/" + deviceName + "/ReporteBancomerMovilAndroid-" + m.getName() + fecha + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @AfterClass
    public void afterClass() {

        //ThreadLocalDriver.getTLDriver().closeApp();
    }

    public void CloseApp() {
        ThreadLocalDriver.getTLDriver().closeApp();
    }

    public void OpenApp() {
        ThreadLocalDriver.getTLDriver().launchApp();
    }

}

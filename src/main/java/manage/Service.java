package manage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.net.URL;


public class Service {

    @BeforeClass
    public static AppiumDriver capability(
            String browserName,
            String deviceName,
            String platformName,
            String platformVersion,
            String udid,
            String automationName,
            String port,
            String wdaLocalPort,
            String appPackage,
            String activity,
            String bundleId,
            String llavero,
            String SystemPort)

            throws IOException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        String automationAppium = "Appium";
        String automation1 = "UiAutomator1";
        String automation2 = "UiAutomator2";

        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("platformName", platformName);
        capabilities.setCapability("platformVersion", platformVersion);
        capabilities.setCapability("udid", udid);
        capabilities.setCapability(MobileCapabilityType.CLEAR_SYSTEM_FILES, true);
        capabilities.setCapability("automationName", automationName);
        capabilities.setCapability("noReset", true);
        capabilities.setCapability("fullReset", false);
      //  capabilities.setCapability("avdLaunchTimeout", "1500000");
        //capabilities.setCapability("deviceReadyTimeout", "1000000");


        if (automationName.equals(automation1) || automationName.equals(automation2) || automationName.equals(automationAppium)) {
            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPackage);
            capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, activity);
            // capabilities.setCapability("appWaitPackage", "com.bancomer.mbanking");
            //  capabilities.setCapability("appWaitActivity", "com.bbva.bgm.loginui.activity.SplashActivity");
            capabilities.setCapability("systemPort", SystemPort);
            //capabilities.setCapability("appWaitDuration", 10000);//this is way optional, 20000 by default
            //capabilities.setCapability("app", "/Users/mtp/Downloads/BBVA-Mexico-QA-glomo.apk");
           // capabilities.setCapability("newCommandTimeout","12000");
            capabilities.setCapability("unicodeKeyboard", true);
            capabilities.setCapability("resetKeyboard", true);
            capabilities.setCapability("autoGrantPermissions", true);
            //capabilities.setCapability("updatedWDABundleId", "com.facebook9000H55.WebDriverAgentRunner9000H55");

            //capabilities.setCapability("skipUnlock", true);
        } else {
            /**skipDeviceInitialization SOLO debe activarse cuando el dispositivo ya tenga la app instalada.**/
            /**Descomentar bundleId solo si la app esta instalada y comentar la capabilitie "app".**/
            /** Skip the installation of io.appium.settings app and the UIAutomator 2 server.**/
            //capabilities.setCapability("skipDeviceInitialization", true);
            //capabilities.setCapability("skipServerInstallation", true);
             capabilities.setCapability("newCommandTimeout","120000");
            //capabilities.setCapability("showXcodeLog", true);
            capabilities.setCapability("bundleId", bundleId);
            // capabilities.setCapability("agentPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent2.xcodeproj");
            //   capabilities.setCapability("xcodeConfigFile", "/Users/mtp/Documents/ProjectSettings.xcconfig");
            capabilities.setCapability("xcodeOrgId", llavero);
            capabilities.setCapability("wdaLocalPort", wdaLocalPort);
            capabilities.setCapability("xcodeSigningId", "iPhone Developer");
            capabilities.setCapability("usePrebuiltWDA", true);
            // capabilities.setCapability("startIWDP",true);
        }
        ThreadLocalDriver.setTLDriver(new AppiumDriver(new URL("http://0.0.0.0:" + port + "/wd/hub"), capabilities));
        return ThreadLocalDriver.getTLDriver();
    }
}

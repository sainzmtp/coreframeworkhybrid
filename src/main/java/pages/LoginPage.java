package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import manage.InitializePage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utilities.Tools;

import static manage.ThreadLocalDriver.getTLDriver;
import static utilities.Tools.*;

import java.util.ArrayList;
import java.util.List;

public class LoginPage extends InitializePage {
    @FindAll({
            @FindBy(id = "heroImage"),
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.widget.Image")
    })
    private ArrayList<MobileElement> bannerBBVA;

    @FindAll({
            @FindBy(xpath = "//android.widget.Button[contains(@text,'cells-molecule-header-right-icon-label')]"),
            @FindBy(xpath = "//XCUIElementTypeButton[@name='cells-molecule-header-right-icon-label']")
    })
    private ArrayList<MobileElement> btnHamburgesa;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View"),
            @FindBy(xpath = "//android.view.View[contains(@text,'BBVA')]"),
            @FindBy(id="titleZone")
    })
    private ArrayList<MobileElement> logoBBVA;

    @FindAll({
            /*@FindBy(xpath = "//android.widget.EditText(@text='')"),
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.widget.EditText"),
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View"),
            @FindBy(xpath = "//android.view.View[[@text = 'Tarjeta de acceso seguro']"),
            @FindBy(xpath = "//android.view.View[contains(@text= 'Tarjeta de acceso seguro')]")
          /*  @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTextField"),
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.widget.EditText"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTextField"),
            @FindBy(xpath = "//android.view.View[contains(@text= 'Tarjeta de acceso seguro')]"),
            @FindBy(xpath = "//*[@text = 'Tarjeta de acceso seguro']"),*/
            @FindBy(id = "input"),
            @FindBy(xpath = "//android.widget.EditText[@resource-id='input']"),
            @FindBy(xpath = "//android.view.View[contains(@text,'Tarjeta de acceso seguro')]"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeTextField")

    })
    private ArrayList<MobileElement> inputTarjetaDeAccesoSeguro;

    @FindAll({
            @FindBy(xpath = "//android.view.View[@resource-id='user-input-pwd']/android.view.View/android.view.View/android.widget.EditText"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeSecureTextField"),
            @FindBy(xpath = "//android.view.View[contains(@text,'Clave de acceso')]")
    })
    private ArrayList<MobileElement> inputClaveDeAcceso;


    @FindAll({
            @FindBy(xpath = "//*[contains(@text,'¿Necesitas ayuda?')]")
    })
    private ArrayList<MobileElement> txtNecesitasAyuda;
    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[3]/android.view.View/android.widget.Button"),
            @FindBy(id = "btnMainIcon")
    })
    private ArrayList<MobileElement> btnVerContrasenia;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.widget.Button"),
            @FindBy(xpath = "//*[@text = 'Borrar campo']")
    })
    private ArrayList<MobileElement> btnCancelarUsuario;

    @FindAll({
            @FindBy(xpath = "//*[@text = 'Entrar']"),
            @FindBy(id = "loginBtn"),
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[4]/android.widget.Button"),
            @FindBy(xpath = "//XCUIElementTypeButton[@name='Entrar']")
    }) private ArrayList<MobileElement> btnEntrar;

    @FindAll({

            @FindBy(xpath = "//android.view.View[contains(@resource-id,'cellsNavigationBar')]/android.view.View"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeLink[1]")
    }) private ArrayList<MobileElement> homeIcon;

    @FindAll({
            @FindBy(id = "btnLogout"),
            @FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnLogout')]"),
            @FindBy(xpath = "//XCUIElementTypeButton[@name='Salir']")
    })private ArrayList<MobileElement> btnLogout;
    @FindAll({
            @FindBy(xpath = "//android.view.View[@text='cells-icon-message-error']/android.widget.Image")
    })private ArrayList<MobileElement> icoAlertaCerrarSesion;

    @FindAll({
            @FindBy(xpath = "//android.view.View[@text='Cierre de sesión")
    })private ArrayList<MobileElement> lblAlertaCerrarSesion;
    @FindAll({
            @FindBy(xpath = "//android.view.View[contains(@text,'BBVA')]")
            //@FindBy(xpath = "")
    })
    private ArrayList<MobileElement> vwBbva;

    @FindAll({
            //@FindBy(xpath = "//android.view.View[contains(@resource-id,'user-input-id')]/android.view.View/android.view.View/android.view.View[contains(@resource-id,'iron-label-0')]")
            @FindBy(xpath = "//android.view.View[contains(@text,'Verifica')]")
            //@FindBy(xpath = "")
    })
    private ArrayList<MobileElement> vwMessageErrorTA;

    @FindAll({
            //@FindBy(xpath = "//android.view.View[contains(@resource-id,'user-input-id')]/android.view.View/android.view.View/android.view.View[contains(@resource-id,'iron-label-0')]")
            @FindBy(xpath = "//android.view.View[contains(@text,'Es necesario')]")
            //@FindBy(xpath = "")
    })
    private ArrayList<MobileElement> vwMessageErrorCA;
    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name=GEMA]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]")
    })
    private ArrayList<MobileElement> txtFecha;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name=navigation]/XCUIElementTypeLink[2]")
    })
    private ArrayList<MobileElement> btnTransferir;


    @FindAll({
            @FindBy(xpath = "//*[@text='Hola']")
    })
    private ArrayList<MobileElement> txtBienvenidaLoginUsuarioRecordado;

    @FindAll({
            @FindBy(xpath = "//*[@text='Cambiar usuario']")
    })
    private ArrayList<MobileElement> txtCambiarUsuairo;

    /*@FindAll({
            @FindBy(xpath = "iron-label-0")
    })
    private ArrayList<MobileElement> txtVerificaNumeroDetarjetaSeaCorrecto;*/

    @FindAll({
            @FindBy(xpath = "iron-label-0"),
            @FindBy(xpath = "//*[@text = 'El usuario o la clave de acceso que has introducido son incorrectos.']")
    })
    private ArrayList<MobileElement> txtElUsuarioOclaveDeAccesoSonIncorrectas;

    @FindAll({
            @FindBy(xpath = "//*[@text = 'Error']")
    })
    private ArrayList<MobileElement> txtError;

    @FindAll({
            @FindBy(xpath = "//*[@text = 'Reintentar']")
    })
    private ArrayList<MobileElement> btnReintentar;

    @FindAll({
            @FindBy(xpath = "//*[@text = 'Cancelar']")
    })
    private ArrayList<MobileElement> btnCancelarAviso;

    @FindAll({
            @FindBy(xpath = "//*[@text = 'Instalar Bnegocios']")
    })

    private ArrayList<MobileElement> btnBnegocios;
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

    private static AppiumDriver<?> driver = getTLDriver();
    Tools tools = new Tools();

    public void validarElementosLoginUsuarioRecordado() {
        waitElementIndividual(logoBBVA.get(0));
        /*isPresentIndividual(txtCambiarUsuairo.get(0));*/
        isPresentIndividual(inputTarjetaDeAccesoSeguro.get(0));
        isPresentIndividual(btnHamburgesa.get(0));
        isPresentIndividual(inputClaveDeAcceso.get(0));
        isPresentIndividual(txtNecesitasAyuda.get(0));
    }

    public void validarElementosLoginUsuarioNuevo() {
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        isPresentIndividual(inputTarjetaDeAccesoSeguro.get(0));
        isPresentIndividual(btnHamburgesa.get(0));
        isPresentIndividual(logoBBVA.get(0));
        isPresentIndividual(inputClaveDeAcceso.get(0));
        isPresentIndividual(txtNecesitasAyuda.get(0));
    }

    public void validarTeclado() {
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputTarjetaDeAccesoSeguro.get(0).click();
    }
    public void validarBtnEntrar(){
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputTarjetaDeAccesoSeguro.get(0).click();
        isDisabled(btnEntrar.get(0));
    }
    public void validarLoginExitosoClienteRecordado(String pass){
        try {
        waitElementIndividual(inputClaveDeAcceso.get(0));
        inputClaveDeAcceso.get(0).setValue(pass);
        inputClaveDeAcceso.get(0).click();
        btnEntrar.get(0).click();
        waitElementIndividual(homeIcon.get(0));
        isPresentIndividual(homeIcon.get(0));
        Assert.assertTrue(true);
        }catch (NoSuchElementException e){
            Assert.assertTrue(false);
        }
    }

    public void validarMensajeDeErrorClaveDeAcceso ()
    {
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        isPresentIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputTarjetaDeAccesoSeguro.get(0).click();
        inputTarjetaDeAccesoSeguro.get(0).setValue("0017803002568630");
        logoBBVA.get(0).click();
        inputClaveDeAcceso.get(0).click();
        inputClaveDeAcceso.get(0).setValue("passwor11");
        btnEntrar.get(0).click();
        isPresentIndividual(btnReintentar.get(0));
        isPresentIndividual(txtElUsuarioOclaveDeAccesoSonIncorrectas.get(0));

    }
    public void validarMensajeDeErrorTarjetaAccesoSeguro()
    {
            waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
            isPresentIndividual(inputTarjetaDeAccesoSeguro.get(0));
            inputTarjetaDeAccesoSeguro.get(0).click();
            inputTarjetaDeAccesoSeguro.get(0).setValue("0017803002568631");
            logoBBVA.get(0).click();
            isPresentIndividual(inputClaveDeAcceso.get(0));
            inputClaveDeAcceso.get(0).click();
            inputClaveDeAcceso.get(0).setValue("password00");
            btnEntrar.get(0).click();
            isPresentIndividual(btnReintentar.get(0));
            isPresentIndividual(txtElUsuarioOclaveDeAccesoSonIncorrectas.get(0));

    }
    public void setUsuario(String user) {
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        isPresentIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputTarjetaDeAccesoSeguro.get(0).click();
        inputTarjetaDeAccesoSeguro.get(0).setValue(user);
        isPresentIndividual(btnCancelarUsuario.get(0));

        /*isPresentIndividual(inputClaveDeAcceso.get(0));
        inputClaveDeAcceso.get(0).click();
        isPresentIndividual(btnVerContrasenia.get(0));
        isPresentIndividual(btnEntrar.get(0));*/
    }
    public void validarTecladoCampoClaveDeAcceso(String pass)
    {
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputClaveDeAcceso.get(0).setValue(pass);
        isDisabled(btnEntrar.get(0));
    }

    public void setPassword(String pass)
    {
        isPresentIndividual(inputClaveDeAcceso.get(0));
        inputClaveDeAcceso.get(0).click();
        inputClaveDeAcceso.get(0).setValue(pass);
        //isPresentIndividual(btnVerContrasenia.get(0));
    }
    public void validarHabiliteBtnEntrar(String user,String pass){
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputTarjetaDeAccesoSeguro.get(0).setValue(user);
        inputClaveDeAcceso.get(0).setValue(pass);
        btnEntrar.get(0).isEnabled();

    }
    public void validaInicioDesesion(String user,String pass){
        waitElementIndividual(inputTarjetaDeAccesoSeguro.get(0));
        inputTarjetaDeAccesoSeguro.get(0).setValue(user);
        inputClaveDeAcceso.get(0).setValue(pass);
        btnEntrar.get(0).isEnabled();
        btnEntrar.get(0).click();
        //VALIDAR POR SI NO ENTRA EL LOGIN POR X O Y.

    }

    public void setTxtUsuario(String user){
        if(inputTarjetaDeAccesoSeguro.get(0).isDisplayed()){
            inputTarjetaDeAccesoSeguro.get(0).click();
            inputTarjetaDeAccesoSeguro.get(0).sendKeys(user);
        }
    }

    public void setTxtPass(String pass){
        if(inputClaveDeAcceso.get(0).isDisplayed()) {
            inputClaveDeAcceso.get(0).click();
            inputClaveDeAcceso.get(0).sendKeys(pass);
        }
    }
    public void tagBbva(){
        if(vwBbva.get(0).isDisplayed()){
            vwBbva.get(0).click();
            Assert.assertTrue(true);
        }
    }

    public void messageErrorTA(){
        Assert.assertTrue(vwMessageErrorTA.get(0).isDisplayed());
    }

    public void menuHamburguesa(){
        Assert.assertTrue(btnHamburgesa.get(0).isDisplayed());
    }

    public void btnEntrar(){
        if(btnEntrar.get(0).isEnabled()){
            Assert.assertTrue(true);
        }else{
            Assert.assertTrue(true);
        }
    }

    public void messageErrorCA(){
        Assert.assertTrue(vwMessageErrorCA.get(0).isDisplayed());
    }

    public void clicBtnEntrarCA(){
        if(btnEntrar.get(0).isDisplayed()){
            btnEntrar.get(0).click();
        }
    }

    public void clickBtnEntrar(){
        try {
            if (btnEntrar.get(0).isDisplayed()) {
                btnEntrar.get(0).click();
                // wait(10000);
                tools.verificarMapeoDeElemento(homeIcon, "homeIcon");
                if (homeIcon.get(0).isDisplayed()) {
                    Assert.assertTrue(true);
                }
            }
        }catch (NoSuchElementException e){
            Assert.assertFalse(true);
        }
    }

    public void IniciarSesion(){
        try {
            waitElementIndividual(logoBBVA.get(0));
            logoBBVA.get(0).click();
            isPresentIndividual(btnEntrar.get(0));
            btnEntrar.get(0).click();
            isPresentIndividual(btnBnegocios.get(0));
            isPresentIndividual(btnCancelarAviso.get(0));
            btnCancelarAviso.get(0).click();
        }catch (NoSuchElementException e){
            Assert.assertFalse(true);
        }
    }

    public void clickBtnHamburgesa(){
        try{
            Thread.sleep(3000);
            btnHamburgesa.get(0).click();

        }catch(NoSuchElementException | InterruptedException e){
            Assert.assertFalse(true);
        }
    }

    public void clickBtnLogout(){
        try{
            if(btnLogout.get(0).isDisplayed()){
                btnLogout.get(0).click();
                if(inputClaveDeAcceso.get(0).isDisplayed()){
                    Assert.assertTrue(true);
                }
            }
        }catch(NoSuchElementException e){
            Assert.assertFalse(true);
        }
    }
    public void validarInactividad(){
        waitTime(300000);
        if (isPresentIndividual(icoAlertaCerrarSesion.get(0))){
            if(isPresentIndividual(lblAlertaCerrarSesion.get(0))){
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }
        }else{
            Assert.assertTrue(false);
        }
    }
}

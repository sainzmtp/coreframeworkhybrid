package pages;

import io.appium.java_client.MobileElement;
import manage.InitializePage;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;
import utilities.Tools;

import javax.xml.xpath.XPath;
import java.util.ArrayList;

import static utilities.Tools.*;

public class DashBoardPage extends InitializePage {
    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]")
    })
    private ArrayList<MobileElement> txtFecha;

    @FindBys({
            @FindBy(xpath = "//XCUIElementTypeOther[@name='navigation']"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeLink[2]")
    })
    private ArrayList<MobileElement> btnTransfeririOS;

    @FindAll({

            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeLink[2]")

    })
    private ArrayList<MobileElement> btnTransferir;

    @FindAll({
            @FindBy(xpath= "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.widget.Button"),
            @FindBy(xpath = "//XCUIElementTypeButton[@name='cells-molecule-header-right-icon-label']"),
            @FindBy(xpath = "//*[contains(@label,'cells-molecule-header-right-icon-labele')]"),
            @FindBy(xpath = "//*[contains(@value,'cells-molecule-header-right-icon-labele')]"),
            @FindBy(xpath = "//*[contains(@name,'cells-molecule-header-right-icon-labele')]")


    })
    private ArrayList<MobileElement> btnHamburgesa;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]"),
            @FindBy(xpath = "//*[contains(@value,'Hola')]")
    })
    private ArrayList<MobileElement> txtDeBienvenida;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[3]"),
            @FindBy(xpath = "(//*[@name='Ver más'])[1]"),


    })
    private ArrayList<MobileElement> primeraCuentaHome;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[4]"),
            @FindBy(xpath = "((//*[@class='UIAView' and ./parent::*[@class='UIAView']]/*[@class='UIAView'])[6]/*[./*[@id='Ver más']])[2]")
    })
    private ArrayList<MobileElement> segundaCuentaHome;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View[1]/android.view.View[2]"),
            @FindBy(xpath = "(//XCUIElementTypeStaticText[@name='Saldo disponible'])[1]")
    })
    private ArrayList<MobileElement> primeraCuentaTransferirOrigen;

    @FindAll({

            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View[3]"),
            @FindBy(xpath = "(//XCUIElementTypeStaticText[@name='Saldo disponible'])[1]")
    })
    private ArrayList<MobileElement> primeraCuentaTranferirDestino;

    @FindAll({
            @FindBy(id = "amountButton"),
            @FindBy(xpath = "(//XCUIElementTypeButton[@name='Aceptar'])[1]")
    })
    private ArrayList<MobileElement> btnAceptarImporte;

    @FindAll({
            @FindBy(id = "label"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[13]")
    })
    private ArrayList<MobileElement> campoImporte;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View[4]/android.view.View[3]/android.view.View/android.view.View/android.widget.EditText"),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[17]/XCUIElementTypeTextField")
    })
    private ArrayList<MobileElement> campoConcepto;

    @FindAll({
            @FindBy(id = "amountButtonOperation"),
            @FindBy(xpath = "//*[@text='Aceptar']"),
            @FindBy(xpath = "//XCUIElementTypeButton[@name='Aceptar']")
    })
    private ArrayList<MobileElement> btnAceptarConcepto;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.widget.Button"),
            @FindBy(xpath = "//*[@text='Aceptar']"),
            @FindBy(xpath = "//XCUIElementTypeButton[@name='Aceptar']")
    })
    private ArrayList<MobileElement> btnAceptarTransferencia;

    @FindAll({
            @FindBy(xpath = ""),
            @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Transferir']")
    })
    private ArrayList<MobileElement> txtTransferir;

    @FindAll({
            @FindBy(xpath = ""),
            @FindBy(xpath = "//XCUIElementTypeOther[@name='banner']/XCUIElementTypeButton")
    })
    private ArrayList<MobileElement> btnCerrar;

    @FindAll({
            @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Transferencia exitosa']"),
            @FindBy(id = "Transferencia exitosa")
    })
    private ArrayList<MobileElement> txtTranferenciaExitosa;

    @FindAll({
            @FindBy(id = "Importe"),
            @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Importe']")
    })
    private ArrayList<MobileElement> txtImporte;
    @FindAll({
            @FindBy(id = "Concepto"),
            @FindBy(xpath = "//XCUIElementTypeStaticText[@name='Concepto']")
    })
    private ArrayList<MobileElement> txtConcepto;

    @FindAll({
            @FindBy(id = "BBVA Bancomer"),
            @FindBy(xpath = "//XCUIElementTypeStaticText[@name='BBVA Bancomer']")
    })
    private ArrayList<MobileElement> txtBBVABancomer;

    @FindAll({
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[10]"),
            @FindBy(xpath = "")
    })
    private ArrayList<MobileElement> cuentaOrigenDetalleDeTransferencia;

    @FindAll({
            @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[12]"),

    })
    private ArrayList<MobileElement> cuentaDestinoDetalleDeTransferencia;

    @FindBy(xpath = "//XCUIElementTypeOther[@name='GEMA']/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]")
            private MobileElement activity;
    //XCUIElementTypeOther[@name="GEMA"]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]

    @FindAll({
            @FindBy(xpath = "//*[@text='Inicio']")
    })
    private ArrayList<MobileElement> txtInicio;

    @FindAll({
            @FindBy(xpath = "//*[@text='Configuracion']")
    })
    private ArrayList<MobileElement> txtConfiguracion;


    @FindAll({
            @FindBy(xpath = "//*[@text='Sucursales y cajeros']")
    })
    private ArrayList<MobileElement> txtSucursalesYcajeros;

    @FindAll({
            @FindBy(xpath = "//*[@text='Acerca de']")
    })
    private ArrayList<MobileElement> txtAcercaDe;
    @FindAll({
            @FindBy(xpath = "//*[@text='Ayuda']")
    })
    private ArrayList<MobileElement> txtAyuda;

    @FindAll({
            @FindBy(xpath = "//*[@text='Salir']")
    })
    private ArrayList<MobileElement> txtSalir;

    @FindAll({
            @FindBy(xpath = "//*[@text = 'Valora la app']")
    })
    private ArrayList<MobileElement> btnValoraApp;

    @FindAll({
            @FindBy(id = "btnRight"),
            @FindBy(xpath = "//*[@text = 'cells-molecule-header-right-icon-label']")
    })
    private ArrayList<MobileElement> btnRight;

    @FindAll({
            @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]")
    })
    private ArrayList<MobileElement> txtTitleUsuario;

    Tools tool = new Tools();

   public void validarElementosMenu(){
        waitElementIndividual(txtTitleUsuario.get(0));
        isPresentIndividual(txtTitleUsuario.get(0));
        isPresentIndividual(btnRight.get(0));
        btnRight.get(0).click();
        isPresentIndividual(txtInicio.get(0));
        isPresentIndividual(txtSucursalesYcajeros.get(0));
        isPresentIndividual(btnValoraApp.get(0));
        isPresentIndividual(txtSalir.get(0));
        txtSalir.get(0).click();
   }


    public void validarElementosMenuHamburgesa(){
        waitElementIndividual(btnHamburgesa.get(0));
        /*btnHamburgesa.get(0).click();*/
        /*isPresentIndividual(txtInicio.get(0));
        isPresentIndividual(txtSucursalesYcajeros.get(0));
        isPresentIndividual(txtAcercaDe.get(0));
        isPresentIndividual(txtAyuda.get(0));
        isPresentIndividual(txtSalir.get(0));*/
        tool.verificarMapeoDeElemento(btnHamburgesa, "btnHamburgesa");
        tool.verificarMapeoDeElemento(txtInicio, "txtInicio");
        tool.verificarMapeoDeElemento(txtSucursalesYcajeros, "txtSucursalesYcajeros");
        tool.verificarMapeoDeElemento(txtAcercaDe, "txtAcercaDe");
        tool.verificarMapeoDeElemento(txtAyuda, "txtAyuda");
        tool.verificarMapeoDeElemento(txtSalir, "txtSalir");
    }
    public void transferenciaCTAtoCTA(){
        //tool.verificarMapeoDeElemento(btnTransferir);
        //tool.verificarMapeoDeElemento(txtFecha);
     /*   tool.verificarMapeoDeElemento(btnHamburgesa,"btnHamburgesa");
        tool.verificarMapeoDeElemento(btnTransferir,"btnTransferiri");
        tool.verificarMapeoDeElemento(txtDeBienvenida,"txtDeBienvenida");
        tool.verificarMapeoDeElemento(primeraCuentaHome,"primeraCuentaHome");*/

        //System.out.println(btnTransferir.get(0).getSize());
       // waitElementIndividual(btnTransferir.get(0));
        waitTime(3000);
        btnTransferir.get(0).click();
        primeraCuentaTransferirOrigen.get(0).click();
        primeraCuentaTranferirDestino.get(0).click();
        campoImporte.get(0).setValue("8");
        btnAceptarImporte.get(0).click();
        campoConcepto.get(0).setValue("BBC");
        btnAceptarConcepto.get(0).click();
        String a= activity.getText();
        btnAceptarTransferencia.get(0).click();
        txtTransferir.get(0).isDisplayed();
        btnCerrar.get(0).isDisplayed();
        txtTranferenciaExitosa.get(0).isDisplayed();
        txtImporte.get(0).isDisplayed();
        txtConcepto.get(0).isDisplayed();
        txtBBVABancomer.get(0).isDisplayed();
        cuentaOrigenDetalleDeTransferencia.get(0).isDisplayed();
        cuentaDestinoDetalleDeTransferencia.get(0).isDisplayed();
    }

    public void funcionBtnSalir() {
        btnCerrar.get(0).click();
        if(txtDeBienvenida.get(0).isDisplayed())
        {
            Assert.assertTrue(true);
        }else
        {
            Assert.assertTrue(false);
        }
    }
}


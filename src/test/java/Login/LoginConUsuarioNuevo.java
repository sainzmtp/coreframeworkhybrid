package Login;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.LoginPage;
import utilities.Data;

public class LoginConUsuarioNuevo extends Data {

    @Test(testName = "LoginConUsuarioNuevo", description = "Validar elementos de la pantalla Login.")
    public void validarElementosUsuarioNuevo() {
        LoginPage login = new LoginPage();
        login.validarElementosLoginUsuarioNuevo();
    }

    @Parameters( "user")
    @Test(description = "validar que se ingrese la tarjeta de acceso seguro")
    public void setearUsuario(String user) {
        LoginPage login = new LoginPage();
        login.setUsuario(user);
    }
    @Parameters( "pass")
    @Test(description = "validar que se ingrese la clave de acceso seguro")
    public void setearPassword(String pass) {
        LoginPage login = new LoginPage();
        login.setPassword(pass);
    }

    @Test(description = "validar  el login exitoso, debe de mostrarse el dashboard.")
    public void validarBtnEntrar() {
        LoginPage login = new LoginPage();
        login.IniciarSesion();
    }


}

package Login;

import org.testng.annotations.Test;
import pages.LoginPage;
import utilities.Data;

public class LoginMensajesDeErro extends Data {

    @Test(testName = "MensajeErrorUser", description = "validar la apertura de la aplicación GEMA con usuario recordado")
    public void validarElementosUsuarioNuevo() {
        LoginPage login2 = new LoginPage();
        login2.validarElementosLoginUsuarioNuevo();
    }

    @Test(description = "validar el mensaje de error al ingresar tarjeta de acceso seguro")
    public void validarMensajeDeErrorAlIngresarTarjetaDeaccesoSeguro() {
        LoginPage login2X = new LoginPage();
        login2X.validarMensajeDeErrorTarjetaAccesoSeguro();
    }
}

package Login;

import org.testng.annotations.Test;
import pages.LoginPage;
import utilities.Data;

public class LoginMensajeDeErrorClaveDeAcceso extends Data {
    @Test(testName = "LoginMensajeDeErrorClaveDeAcceso", description = "validar la apertura de la aplicación GEMA con usuario recordado")
    public void validarElementosUsuarioRecordado() {
        LoginPage login2 = new LoginPage();
        login2.validarElementosLoginUsuarioRecordado();
    }

    @Test(description = "validar el mensaje de error al ingresar clave de acceso seguro erroneamente")
    public void validarMensajeDeErrorAlIngresarTarjetaDeaccesoSeguro() {
        LoginPage login2 = new LoginPage();
        login2.validarMensajeDeErrorClaveDeAcceso();
    }
}

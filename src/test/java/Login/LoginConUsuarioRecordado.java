package Login;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.LoginPage;
import utilities.Data;

public class LoginConUsuarioRecordado extends Data {

    @Test(testName = "LoginConUsuarioRecordado", description = "validar la apertura de la aplicación GEMA con usuario recordado")
    public void validarElementosUsuarioRecordado() {
        LoginPage login2 = new LoginPage();
        login2.validarElementosLoginUsuarioRecordado();
    }

    @Parameters("user")
    @Test(description = "Ingresar numero de usuario")
    public void setearUsuario(String user) {
        LoginPage login2 = new LoginPage();
        login2.setUsuario(user);
    }

    @Parameters("pass")
    @Test(description = "Ingresar contraseña de usuario")
    public void setearPassword(String pass) {
        LoginPage login2 = new LoginPage();
        login2.setPassword(pass);
    }

    @Test(description = "validar  el login exitoso, debe de mostrarse el dashboard.")
    public void IniciaSesion() {
        LoginPage login2 = new LoginPage();
        login2.IniciarSesion();
    }

    @Test(description = "cerrar y abrir el app")
    public void cerrarAbrirApp(){
        Data data = new Data();
        data.CloseApp();
        data.OpenApp();
    }

    @Parameters("pass")
    @Test(description = "Login con usuario nuevo")
    public void setearPassword2(String pass) {
        LoginPage loginPage = new LoginPage();
        loginPage.setPassword(pass);
    }

    @Test(description = "Inicia sesion con usuario recordado")
    public void IniciarSesion2() {
        LoginPage login2 = new LoginPage();
        login2.IniciarSesion();
    }

}


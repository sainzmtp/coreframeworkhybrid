package Login;

import OwnTransfer.DashBoard;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DashBoardPage;
import pages.LoginPage;
import utilities.Data;

public class LoginCierreDeSesion extends Data {

    @Test(testName = "LoginYCierreSesion", description = "validar  el login exitoso, debe de mostrarse el dashboard.")
    public void validarElementosUsuarioRecordado() {
        LoginPage login = new LoginPage();
        login.validarElementosLoginUsuarioNuevo();
    }

    @Parameters("user")
    @Test(description = "Se establese el numero de usuario")
    public void setUsuario(String user) {
        LoginPage loginPage = new LoginPage();
        loginPage.setUsuario(user);
    }

    @Parameters("pass")
    @Test(description = "Se establece password de usuario")
    public void setPassword(String pass) {
        LoginPage loginPage = new LoginPage();
        loginPage.setPassword(pass);

    }

    @Test(description = "Se inicia sesion con usuario y contraseña")
    public void iniciarSesion() {
        LoginPage loginPage = new LoginPage();
        loginPage.IniciarSesion();
    }

    @Test(description = "validar que al cerrar sesión se muestre el usuario recordado")
    public void validarCerarSesionConUsuarioRecordado() {
        DashBoardPage dash = new DashBoardPage();
        LoginPage log2 = new LoginPage();
        dash.validarElementosMenu();
        log2.validarElementosLoginUsuarioRecordado();
    }
}

package OwnTransfer;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DashBoardPage;
import pages.LoginPage;
import utilities.Data;

public class DashBoard extends Data {
    @Parameters({"pass"})
    @Test(testName = "CP001", description = "Validar login exitoso con un cliente recordado.")
    public void validarLoginExitosoClienteRecordado(String pass) {
        LoginPage log2x = new LoginPage();
        log2x.validarLoginExitosoClienteRecordado(pass);
    }

    @Test(description = "Validar una 'Transferencia Exitosa' cuenta AHORRO BBVA a una cuenta AHORRO de BBVA")
    public void validarTransferenciaExitosaCTA() {
        DashBoardPage dash = new DashBoardPage();
        dash.transferenciaCTAtoCTA();
    }

    @Test(description = " Validar que al hacer tap en el boton 'Salir' regrese a la pantalla de dashboard")
    public void validarFuncionamientoBtnSalir() {
        DashBoardPage dash = new DashBoardPage();
        dash.funcionBtnSalir();
    }
}
